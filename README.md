# AuthoritySysOfflineH5

#### 介绍
一个基于VUE-admin+element+Vuex的权限管理系统模型

#### 软件架构
核心还是VUE，其中element提供了UI部分的操作，而VUE-admin在此基础上封装了更多功能，如果说vue+element是小米加步枪，那么VUE-admin就是95式了！个人之见，VUE-admin梳理了大部分offline的业务场景，增加了一些开发辅助框架，比如Vuex，本地跨域代理服务等。有了这些，开发人员只需要套套子就行了，当然，实际的开发中，也没有套套子那么轻松。
本框架重点解决的是权限管理难题，众所周知，当一个庞大的系统有了权限管理的需求，真是要了程序员的命。目前网上的一些关于权限管理的经验大多是jQuery时代，基于服务端渲染的。而以后的趋势是offline也玩前后端分离，那么在前后端分离的场景下如何搞权限管理呢？嗯，这个框架提供了一个思路，欢迎各位抛砖引玉。

#### 安装教程

老规矩：`npm install`
`npm run dev`

#### 使用说明

1.  该框架使用了最新的element版本，修复了一些奇怪的bug，增加了功能，关于这一点，可以参见 ["elentment更新日志"](https://element.eleme.cn/#/zh-CN/component/changelog)
2.  权限系统的核心实现原理是依赖VUE允许注册自定义指令的能力。更细节来说，是使用 directives 选项来注册了一个名为v-permission的局部指令。接口返回的权限数据首先传入Vuex，好处是，如果用户在操作的过程中发生权限变更，只要服务端发出新权限数据，能够马上同步到客户端。该数据也被页面上的v-permission捕获，从而判断按钮和页面是否允许被操作/展示。
3. 对于需要权限控制的按钮或路由，需要在编码时为其赋予唯一Key，该Key需要和服务端同步，以便权限系统能够识别，如下图所示。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0225/083152_4dfe5b34_1398991.png "微信截图_20200225080757.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0225/083205_9eeb4ea5_1398991.png "微信截图_20200225080948.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0225/083221_d3ee1343_1398991.png "微信截图_20200225081136.png")
4. 该框架自带跨域serve

#### 参与贡献

如有疑问，欢迎对线~
