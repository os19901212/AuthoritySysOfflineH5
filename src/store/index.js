import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'

Vue.use(Vuex)

// https://webpack.js.org/guides/dependency-management/#requirecontext
const modulesFiles = require.context('./modules', false, /\.js$/)

// you do not need `import app from './modules/app'`
// it will auto require all vuex module from modules file
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  // set './app.js' => 'app'
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  const value = modulesFiles(modulePath)
  modules[moduleName] = value.default
  return modules
}, {})

const store = new Vuex.Store({
  modules,
  getters,
  state: {
    house: {

      imgKinds: [
        { label: '房型图', id: 1 },
        { label: '卧室', id: 2 },
        { label: '客厅', id: 3 },
        { label: '阳台', id: 4 },
        { label: '厨房', id: 5 },
        { label: '餐厅', id: 6 },
        { label: '外立面', id: 7 },
        { label: '书房', id: 8 },
        { label: '花园', id: 9 },
        { label: '车库', id: 10 },
        { label: '大堂', id: 11 },
        { label: '电梯厅', id: 12 },
        { label: '办公室', id: 13 },
        { label: '办公区域', id: 14 },
        { label: '走廊', id: 15 },
        { label: '周边环境', id: 16 }
      ],
      priceList: [
        { label: '200万以下', id: 1, low: 0, hiehgt: 199 },
        { label: '200-400万', id: 2, low: 200, hiehgt: 400 },
        { label: '400-600万', id: 3, low: 400, hiehgt: 600 },
        { label: '600-800万', id: 4, low: 600, hiehgt: 800 },
        { label: '800万以上', id: 5, low: 800, hiehgt: 99999 }
      ],
      squareList: [
        { label: '70m²以下', id: 1, low: 0, hiehgt: 70 },
        { label: '70-90m²', id: 2, low: 70, hiehgt: 90 },
        { label: '90-110m²', id: 3, low: 90, hiehgt: 110 },
        { label: '110-150m²', id: 4, low: 110, hiehgt: 150 },
        { label: '150m²以上', id: 5, low: 150, hiehgt: 99999 }
      ],
      liveState: [{ label: '未知', id: 0 }, { label: '租房', id: 1 }, { label: '有房', id: 2 }],
      marriage: [{ label: '未知', id: 0 }, { label: '未婚', id: 1 }, { label: '已婚', id: 2 }],
      sex: [{ label: '未知', id: 0 }, { label: '男', id: 1 }, { label: '女', id: 2 }],
      face: [{ label: '东', id: 1 }, { label: '西', id: 2 }, { label: '南', id: 3 }, { label: '北', id: 4 }, { label: '东南', id: 5 }, { label: '东北', id: 6 }, { label: '西南', id: 7 }, { label: '西北', id: 8 }],
      shape: [{ label: '飞机户型', id: 1 }, { label: '南北通', id: 2 }, { label: '朝南', id: 3 }, { label: '朝北', id: 4 }, { label: '朝西', id: 5 }, { label: '朝东', id: 6 }],
      roomCount: [{ label: '1室', id: 1 }, { label: '2室', id: 2 }, { label: '3室', id: 3 }, { label: '4室', id: 4 }, { label: '4室及以上', id: 5 }],
      hallCount: [{ label: '1厅', id: 1 }, { label: '2厅', id: 2 }, { label: '3厅', id: 3 }, { label: '3厅及以上', id: 4 }],
      toiletCount: [{ label: '1卫', id: 1 }, { label: '2卫', id: 2 }, { label: '3卫', id: 3 }, { label: '3卫及以', id: 4 }],
      state: [{ label: '未售-无租', id: 1 }, { label: '未售-在租', id: 2 }, { label: '已售', id: 3 }, { label: '未知', id: 4 }, { label: '下架', id: 9 }],
      decorate: [{ label: '豪装', id: 1 }, { label: '精装', id: 2 }, { label: '简装', id: 3 }, { label: '毛坯', id: 4 }],
      keyState: [{ label: '有', id: 1 }, { label: '无', id: 2 }, { label: '需借', id: 3 }],
      propertyNature: [{ label: '商品房', id: 1 }, { label: '动迁房', id: 2 }, { label: '公寓', id: 3 }, { label: '办公楼', id: 4 }, { label: '商铺', id: 5 }],
      propertyYear: [{ label: '40年', id: 1 }, { label: '50年', id: 2 }, { label: '70年', id: 3 }],
      houseKind: [{ label: '普通房屋', id: 1 }, { label: '学区房', id: 2 }],
      picVideo: [{ label: '有图片', id: 3 }, { label: '有视频', id: 2 }, { label: '无', id: 0 }],
      source: [{ label: '上门', id: 1 }, { label: '网络', id: 2 }, { label: '驻守', id: 3 }, { label: '老客户', id: 4 }, { label: '卖房转定', id: 5 }],
      watchWay: [{ label: '提前预约', id: 1 }, { label: '有钥匙', id: 2 }, { label: '自主', id: 3 }, { label: '租客', id: 4 }],
      uniqueEntrust: [{ label: '是', id: 1 }, { label: '否', id: 2 }],
      onlyHouse: [{ label: '是', id: 1 }, { label: '否', id: 2 }],
      houseLife: [{ label: '不满二年', id: 1 }, { label: '满二年', id: 2 }, { label: '满五年', id: 3 }],
      stair: [{ label: '1', id: 1 }, { label: '2', id: 2 }, { label: '3', id: 3 }, { label: '4', id: 4 }, { label: '5', id: 5 }, { label: '6', id: 6 }],
      door: [{ label: '1', id: 1 }, { label: '2', id: 2 }, { label: '3', id: 3 }, { label: '4', id: 4 }, { label: '4以上', id: 5 }]
    }
  }
})

export default store
