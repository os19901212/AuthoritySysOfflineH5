import request from '@/utils/request'

// 获取客户详情
export function getCustommerDetail(params) {
  return request({
    url: '/custommer/detail',
    method: 'get',
    params: params
  })
}
// 更新客户信息，如果不传id号，则为新增
export function updateCustommer(data) {
  return request({
    url: '/custommer/update',
    method: 'post',
    data
  })
}
// 获取客户列表
export function getCustommerList(params) {
  return request({
    url: '/custommer/list',
    method: 'get',
    params: params
  })
}

// 获取推荐的房源
export function recommendHouse(params) {
  return request({
    url: '/custommer/recommendhouse',
    method: 'get',
    params: params
  })
}

// 删除客户
export function deleteCustommer(data) {
  return request({
    url: '/custommer/deletecustommer',
    method: 'post',
    data
  })
}
// 呼叫客户电话
export function callCustomer(data) {
  return request({
    url: '/call/creat',
    method: 'post',
    data
  })
}
// 发送呼叫备注
export function submitCallNote(data) {
  return request({
    url: '/call/note',
    method: 'post',
    data
  })
}
// 获取呼叫列表
export function getCallList(params) {
  return request({
    url: '/call/list',
    method: 'get',
    params: params
  })
}
