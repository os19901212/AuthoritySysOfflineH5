import request from '@/utils/request'

// 获取小区详情
export function getCommunityDetail(params) {
  return request({
    url: '/community/detail',
    method: 'get',
    params: params
  })
}
// 更新小区信息，如果不传id号，则为新增
export function updateCommunityDetail(data) {
  return request({
    url: '/community/update',
    method: 'post',
    data
  })
}
// 获取小区列表
export function getCommunityList(params) {
  return request({
    url: '/community/list',
    method: 'get',
    params: params
  })
}
// 获取学校列表
export function getSchoolList(params) {
  return request({
    url: '/community/schoollist',
    method: 'get',
    params: params
  })
}
// 获取一个城市的所有板块列表
export function getCityAllBoard(params) {
  return request({
    url: '/community/allboard',
    method: 'get',
    params: params
  })
}
