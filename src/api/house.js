import request from '@/utils/request'

// 获取房源详情
export function getHouseDetail(params) {
  return request({
    url: '/house/detail',
    method: 'get',
    params: params
  })
}
// 添加房源
export function addHouseDetail(data) {
  return request({
    url: '/house/add',
    method: 'post',
    data
  })
}
// 更新房源信息，如果不传id号，则为新增
export function updateHouseDetail(data) {
  return request({
    url: '/house/update',
    method: 'post',
    data
  })
}
// 获取房源列表
export function getHouseList(params) {
  return request({
    url: '/house/list',
    method: 'get',
    params: params
  })
}
// 获取房源调价记录
export function changePriceLog(params) {
  return request({
    url: '/house/changepricelog',
    method: 'get',
    params: params
  })
}
// 获取推荐的客户
export function recommendCustomer(params) {
  return request({
    url: '/house/recommendcustomer',
    method: 'get',
    params: params
  })
}
// 获取浏览记录
export function seeLog(params) {
  return request({
    url: '/house/seelog',
    method: 'get',
    params: params
  })
}
// 下架房源
export function unUseHouse(data) {
  return request({
    url: '/house/unusehouse',
    method: 'post',
    data
  })
}

// 修改房源价格
export function changePrice(data) {
  return request({
    url: '/house/changeprice',
    method: 'post',
    data
  })
}
// 查看房屋门牌号
export function seeRoomNum(data) {
  return request({
    url: '/house/seeroomnum',
    method: 'post',
    data
  })
}
// 获取上传到七牛服务器的token，支持一次获取多个
export function getQiNiuToken(data) {
  return request({
    url: '/house/seeroomnum',
    method: 'post',
    data
  })
}
