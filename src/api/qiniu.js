import request from '@/utils/request'
// 获取上传到七牛服务器的token，支持一次获取多个
export function getQiNiuToken(data) {
  return request({
    url: '/common/upload-token',
    method: 'post',
    data
  })
}
