import request from '@/utils/request'

// 机构列表
export function getOrganList(params) {
  return request({
    url: '/system/organ/organ-list',
    method: 'get',
    params: params
  })
}

// 新增机构
export function createOrgan(data) {
  return request({
    url: '/system/organ/create-organ',
    method: 'post',
    data
  })
}

// 重命名
export function updateOrgan(data) {
  return request({
    url: '/system/organ/update-organ',
    method: 'post',
    data
  })
}

// 删除机构
export function deleteOrgan(data) {
  return request({
    url: '/system/organ/delete-organ',
    method: 'post',
    data
  })
}
