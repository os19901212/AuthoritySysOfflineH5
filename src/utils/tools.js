import axios from 'axios'
export function randomString(len) {
  len = len || 32
  var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678' /** **默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
  var maxPos = $chars.length
  var pwd = ''
  for (var i = 0; i < len; i++) {
    pwd += $chars.charAt(Math.floor(Math.random() * maxPos))
  }
  return pwd
}

// 执行上传到七牛方法，供其它方法调用
export function sendToQiNiu(formdata, getDatas, process) {
  axios({
    method: 'POST',
    url: 'http://upload.qiniu.com',
    data: formdata,
    headers: { 'Content-Type': 'multipart/form-data' },
    onUploadProgress: function(progressEvent) {
      // 原生获取上传进度的事件
      if (progressEvent.lengthComputable) {
        process(progressEvent)
        // console.log(progressEvent)
        // 属性lengthComputable主要表明总共需要完成的工作量和已经完成的工作是否可以被测量
        // 如果lengthComputable为false，就获取不到progressEvent.total和progressEvent.loaded
        // callback1(progressEvent);
      }
    }
  }).then(function(res) {
    res.qiniuaddr = 'http://qn.fnyue.com/'
    getDatas(res)
  })
}
